const campoMensagens = document.querySelector('.mensagens');
const inputMensagem = document.querySelector('.texto');
const botaoEnviar = document.querySelector('.enviar');

botaoEnviar.addEventListener('click', () => {

  campoMensagens.innerHTML += `
  <div class="mensagem">
    <p class="texto-mensagem">${inputMensagem.value}</p>
    <div class="botoes-mensagem">
      <button class="editar">Editar</button>
      <button class="excluir">Excluir</button>
    </div>
  </div>
  `
  excluirMensagem();
  editarMensagem();
  inputMensagem.value = '';
});

function excluirMensagem() {

  const botaoExcluir = document.querySelectorAll('.excluir');

  botaoExcluir.forEach(botao => {

    botao.addEventListener('click', (event) => {
      let divMensagem = event.target.parentElement.parentElement;
      divMensagem.classList.add('excluido');
    });
  });
}

function editarMensagem() {
  const botaoEditar = document.querySelectorAll('.editar');

  botaoEditar.forEach(botao => {

    botao.addEventListener('click', (event) => {

      let divMensagem = event.target.parentElement.parentElement;
      let mensagem = divMensagem.querySelector('.texto-mensagem');
      let novaMensagem = prompt('Edite sua mensagem', `${mensagem.textContent}`);
      mensagem.innerText = novaMensagem; 
    });
  });
}
